let txt = 'Etant en première année d’un Bachelor Informatique j’ai eu l’occasion de développer mes compétences sur des technologies tel que Java, Python, C# ou encore HTML, CSS, PHP concernant le web. J’ai également l’habitude d’évoluer dans un environnement Agile que je considère selon moi indispensable quand à la bonne réalisation d’un projet.';
let speed = 45;
let i = 0; 

function description() {
    let content = document.getElementById('description_content');
    let button = document.getElementById('description_title'); 
    if (content.style.display != "none") {

        content.style.display = "block";
        button.style.marginBottom = "5%";
        button.classList.add("rebond");
               
        if (i < txt.length) {
            document.getElementById("description_content").innerHTML += txt.charAt(i);
            i++;
            setTimeout(description, speed);
        }
    } 
    else 
    {
        content.style.display = "block";
    }
}

async function run_after(cb, time) {
    return new Promise(done => 
    setTimeout(() => {
        cb();
        done();
    }, time))
}

function show_skill(id){
    let skill = document.getElementById(id);
    skill.style.display = "block";
    //ajouter une class plutot que display block avec dans mon css une class avec MAXheight 0  et overflow hidden et le setter a 50px et avec une transition
}


// Makeshift carousel function that gets invoked with the Index to start it off, then the callback increments the index to recursively invoke the same function. Works even in IE11!
let testimonialItems = document.querySelectorAll(".item label");
let timer;
function cycleTestimonials(index) {
   timer = setTimeout(function() {
    let evt;
    if (document.createEvent){
      //If browser = IE, then polyfill
      evt = document.createEvent('MouseEvent');
      evt.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    } else {
      //If Browser = modern, then create new MouseEvent
      evt = new MouseEvent("click", {
            view: window,
            bubbles: true,
            cancelable: true,
            clientX: 20
          });
    }
    let ele = "." + testimonialItems[index].className;
    let ele2 = document.querySelector(ele)
    ele2.dispatchEvent(evt);
    index++; // Increment the index
    if (index >= testimonialItems.length) {
      index = 0; // Set it back to `0` when it reaches `3`
    }
    cycleTestimonials(index); // recursively call `cycleTestimonials()`
    document.querySelector(".testimonials").addEventListener("click", function() {
      clearTimeout(timer); //stop the carousel when someone clicks on the div
    });
  }, 4000); //adjust scroll speed in miliseconds
}
//run the function
cycleTestimonials(0);


myID = document.getElementById("myID");

var myScrollFunc = function () {
    var y = window.scrollY;
    if (y >= 1500) {
      myID.className = "skills_container show"
    } else {
      myID.className = "skills_container hide"
    }
};

window.addEventListener("scroll", myScrollFunc);